var webpack = require('webpack'); 
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');  //自动生成HTML模板
var OpenBrowserPlugin = require('open-browser-webpack-plugin');  //编译完成之后自动打开浏览器

var config = require('./server.config');
var port = config.port;  //端口号
var getDeviceIP = config.getDeviceIP //ip

module.exports = {
    // entry: path.resolve(__dirname, 'src/index.jsx'),
    entry:{
		app: './src/index.jsx',
	},
    output: {
        path: __dirname+'/build',
        filename: 'bundle.[hash].js',
        publicPath: 'http://' + getDeviceIP() + ':' + port + '/'  //表示网站的上线地址，而publicPath就需要设置为”/”,表示当前路径
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use:['style-loader', 'css-loader']
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [require('autoprefixer')];
                            }
                        }
                    },
                    'less-loader'
                ]
            },
            {
                test: /\.(jpg|png|gif|ttf|eot|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: '8000',
                            name: 'images/[name].[ext]'
                        }
                    }
                ],
            },
            {
                test: /\.svg$/,
                use: [
                    'svg-sprite-loader',
                    'svg-fill-loader',
                    'svgo-loader'
                ]
			},	
            {
                test: /\.(js|jsx)$/, // test 去判断是否为.js或.jsx,是的话就是进行es6和jsx的编译
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: [
                        ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }], // `style: true` 会加载 less 文件
                        ["transform-runtime",   //代替polyfill，使用 promise,set,map，generator 不报错
                            {
                                "helpers": true,
                                "polyfill": true,
                                "regenerator": true,
                                "moduleName": "babel-runtime"
                            }
                        ]
                    ]  
                }
            }
        ]
    },
    // resolve: {  //一直报react-native错误，是因为顺序不对
    //     extensions: ['.js', '.jsx', '.less','.css','.web.js']
    // },
    resolve: {
        extensions: ['.less', '.web.js', '.js', '.jsx', '.css']
    },
    devtool: 'source-map',
    plugins: [
        //自动生成HTML模板插件
        new HtmlWebpackPlugin({
            template: __dirname + '/src/static/index.html',
            favicon: 'favicon.ico'
        }),
        //热更新
        new webpack.HotModuleReplacementPlugin(),

        //服务启动后自动开启浏览器
        new OpenBrowserPlugin({  
            url: 'http://' + getDeviceIP() + ':' + port
        }),

        // 可在业务 js 代码中使用 __DEV__ 判断是否是dev模式（dev模式下可以提示错误、测试报告等, production模式不提示）
        new webpack.DefinePlugin({
            __DEV__: JSON.stringify(JSON.parse((process.env.NODE_ENV == 'dev') || 'false'))
        })
    ],

    devServer: {
        proxy: {
            // 凡是 `/api` 开头的 http 请求，都会被代理到 localhost:3000 上，由 koa 提供 mock 数据。
            '/api': {
                // target: 'http://' + getDeviceIP() + ':5555',
                target: 'http://localhost:5555/',
                secure: false
            }
        },
        contentBase: "./public", //本地服务器所加载的页面所在的目录
        historyApiFallback: true, //不跳转
        inline: true, //实时刷新
        hot: true,  // 使用热加载插件 HotModuleReplacementPlugin
        port: port,
        host: getDeviceIP()
    },   
    stats: {   //终端中输出结果为彩色
        colors: true
    },
}