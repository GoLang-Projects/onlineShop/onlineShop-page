//配置webpack与electron服务
var os = require('os');  //获取系统信息

module.exports = { //es5导出对象
    port: '7777',
    getDeviceIP: function(){  //获取设备ip
        var localhost = ''
        try {
            var network = os.networkInterfaces();
            localhost = network['以太网'] && network['以太网'][1].address || network['WLAN'] && network['WLAN'][1].address
        } catch (e) {
            localhost = 'localhost';
        }
        return localhost
    }
}

