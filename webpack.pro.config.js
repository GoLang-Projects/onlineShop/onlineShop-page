var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin'); // 删除文件

module.exports = {
    entry: {
        src: path.resolve(__dirname, 'src/index.jsx'),
        vendor: [ //将第三方依赖单独打包
            'react',
            'react-dom'
        ]
    },
    output: {
        path: __dirname + '/build',
        filename: "[name].[chunkhash:8].js"
    },
    module: {
        rules:[
            {
                test: /\.less$/,
                use:ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use:['css-loader','less-loader', {loader:'postcss-loader',options: {plugins: function() {return [require('autoprefixer')];} }}]
                })
            },
            {
                test: /\.css$/,
                use:['style-loader', 'css-loader']
            },
            {
                test: /\.(jpg|png|gif|svg|ttf|eot|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: '8000',
                            name: 'images/[name].[ext]'
                        }
                    }
                ],
            },
            {
                test: /\.(js|jsx)$/, // test 去判断是否为.js或.jsx,是的话就是进行es6和jsx的编译
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: [
                        ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }], // `style: true` 会加载 less 文件
                        ["transform-runtime",   //代替polyfill，使用 promise,set,map，generator 不报错
                            {
                                "helpers": true,
                                "polyfill": true,
                                "regenerator": true,
                                "moduleName": "babel-runtime"
                            }
                        ]
                    ]  
                }
            },
        ]
    },
    resolve:{
        extensions:['.js','.jsx','.less','.json']
    },
    plugins: [
        //webpack内置的banner-plugin
        new webpack.BannerPlugin('深圳奥琦玮-云POS'),

        //html模板
        new HtmlWebpackPlugin({
            template: __dirname + '/src/assets/index.html',
            // favicon: 'favicon.ico' 
        }),
        
        //定义为生产环境，编译React时压缩到最小
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),

        // 删除打包生成的文件插件
        new CleanWebpackPlugin(['build'],{  //build文件夹名
            root: path.resolve(__dirname),
            verbose: true,
            dry: false
        }),

        //丑化代码
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),

        //分离css和js文件
        new ExtractTextPlugin('[name].[chunkhash:8].css'), 

         // 提供公共代码
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'js/[name].[chunkhash:8].js'
        })
    ]
}