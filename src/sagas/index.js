import createSagaMiddleware, { takeLatest,  } from 'redux-saga';

const test = (time) => {
    setTimeout(() => {
        console.log('saga')
    }, time)
}
export function* rootSaga(){
    yield[
        // takeLatest()
        test(1000)
    ]
}

const sagaMiddleware = createSagaMiddleware();

export default sagaMiddleware;