import React, { Compoennt } from 'react';
import { render } from 'react-dom';
import { Button } from 'antd';
import RouterMap from './routers';
import store from './stores';
import "./style";

render(
    <RouterMap store={store} />, 
    document.querySelector('#root')
);

