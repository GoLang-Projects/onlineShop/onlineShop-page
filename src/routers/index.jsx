import React, { Component } from 'react';
import { BrowserRouter, HashRouter, Router, Route, Switch, Redirect } from 'react-router-dom';
import Login from '../containers/Login';
import Home from '../containers/Home';
import NotFount from '../containers/NotFount';

export default class RouterMap extends Component {
    render(){
        return(
            <HashRouter>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/login" component={Login} />
                    <Route path="*" component={NotFount} />
                </Switch>
            </HashRouter>
        )
    }
}


