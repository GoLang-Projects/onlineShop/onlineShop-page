import axios from 'axios'
import qs from 'qs'


const initParam = {
	url: '',
	params: {},
	timeout: 3000,
	mes: '请求超时',
	format: 'json'  //默认传给后台的格式是json，可以通过stringify将格式转为字符串  qs.stringify(params)
}
const Post = (param) => {
	let { url, params, timeout, mes, format } = Object.assign({}, initParam, param);
	if(format === 'string'){
		params = qs.stringify(params)
	}else if(format !== 'json'){
		console.log('format值只能为json和string，默认值为json')
	}
	return axios.post(url, params, {timeout:timeout}).then(res =>{
		return	res.data
	}).catch((err)=>{
		var isTimeOut = /timeout/.test(err);
		if(isTimeOut){
			alert(mes);
		}else {
			console.log(err)
		}
	})
}

const Get = (param) => {
	const { url, params, timeout, mes } = Object.assign({}, initParam, param);
	return axios.get(url, {params:params}, {thimeout:timeout}).then(res => {
		return res.data
	}).catch((err)=>{
		var isTimeOut = /timeout/.test(err);
		if(isTimeOut){
			alert(mes);
		}else {
			console.log(err)
		}
	})
}

export { Post, Get }
