import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import './style'
const FormItem = Form.Item;
const logo = require("../../static/img/logo.jpg")
import { Post } from '../../utils/fetch'

class LoginForm extends Component {
	handleSubmit = (e) => {
		e.preventDefault();
		console.log(e.target)
		this.props.form.validateFields((err, values) => {
			if (!err) {
				const { username, password } = values
				const data = Post({
					url: '/api/user/login',
					params: { username, password },
					mes: '注册超时'
				})
				console.log(data, 19999999)
			}
		});
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<Row className="login-container">
				<Col xs={23} sm={20} md={11} xl={9}  className="login-main">
					<h1 className="logo">
						<img src={logo} alt=""/>
						<p>GoGoGo</p>
					</h1>
					<Form onSubmit={this.handleSubmit} className="login-form">
						<FormItem>
							{getFieldDecorator('username', {
								rules: [{ required: true, message: '请输入用户名!' }],
							})(
								<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="请输入用户名!" />
							)}
						</FormItem>
						<FormItem>
							{getFieldDecorator('password', {
								rules: [{ required: true, message: '请输入密码!' }],
							})(
								<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="请输入密码!" />
							)}
						</FormItem>
						<FormItem>
							{getFieldDecorator('remember', {
								valuePropName: 'checked',
								initialValue: true,
							})(
								<Checkbox>记住密码</Checkbox>
							)}
							<a className="login-form-forgot" href="javascript:;">忘记密码</a>
							<Button type="primary" htmlType="submit" className="login-form-button login-btn">
								登录
							</Button>
							<Button type="" htmlType="submit" className="login-form-button">
								注册
							</Button>
						</FormItem>
					</Form>
				</Col>
				<Col className="login-footer">Copyright <Icon type=" anticon-copyright" /> 2018 狗狗狗</Col>
			</Row>
		);
	}
}

const WrappedLoginForm = Form.create()(LoginForm);
export default WrappedLoginForm;