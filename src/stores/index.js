import { createStore, applyMiddleware, compose } from 'redux';
import reducers from '../reducers';
import sagaMiddleware, { rootSaga } from '../sagas';

let store;
if(!(window.__REDUX_DEVTOOLS_EXTENSION__ || window.__REDUX_DEVTOOLS_EXTENSION__)){
    store = createStore(
        reducers,
        applyMiddleware(sagaMiddleware)
    );
}else{
    store = createStore(
        reducers,
        compose(applyMiddleware(sagaMiddleware),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()) //插件调试，未安装会报错
    );
}

sagaMiddleware.run(rootSaga);

export default store;